package cellular;

import datastructure.CellGrid;
import datastructure.IGrid;

import java.util.Random;

public class BriansBrain implements CellAutomaton {

    IGrid currentGeneration;

    /**
     *
     * Construct a Game Of Life Cell Automaton that holds cells in a grid of the
     * provided size
     *
     * @param height The height of the grid of cells
     * @param width  The width of the grid of cells
     */

    public BriansBrain(int rows, int columns) {
        currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
        initializeCells();
    }

    @Override
    public void initializeCells() {
        Random random = new Random();
        for (int row = 0; row < currentGeneration.numRows(); row++) {
            for (int col = 0; col < currentGeneration.numColumns(); col++) {
                if (random.nextBoolean()) {
                    currentGeneration.set(row, col, CellState.ALIVE);
                } else {
                    currentGeneration.set(row, col, CellState.DEAD);
                }
            }
        }
    }

    @Override
    public int numberOfRows() {
        return currentGeneration.numRows();
    }

    @Override
    public int numberOfColumns() {
        return currentGeneration.numColumns();
    }

    @Override
    public CellState getCellState(int row, int col) {

        return currentGeneration.get(row, col);
    }

    @Override
    public void step() {
        IGrid nextGeneration = currentGeneration.copy();
        for(int row = 0;row<this.numberOfRows();row++){
            for(int col = 0;col<this.numberOfColumns();col++) {
                nextGeneration.set(row, col, nextGeneration.get(row, col));
            }
        }
        for(int row = 0;row<this.numberOfRows();row++){
            for(int col = 0;col<this.numberOfColumns();col++) {
                currentGeneration.set(row, col, nextGeneration.get(row, col));

            }
        }
    }

    @Override
    public CellState getNextCell(int row, int col) {   //En død celle med akkurat 2 levende naboer blir levende
        CellState state = currentGeneration.get(row, col);

        int alive = this.countNeighbors(row, col, CellState.ALIVE);


        if (state == CellState.ALIVE) {
            return CellState.DYING;
        }else if (state == CellState.DYING) {  //En død celle med nøyaktig tre levende naboer blir levende.
            return CellState.DEAD;

        }else  {
            if (alive == 2)
                return CellState.ALIVE;
            else{
                return CellState.DEAD;
            }

        }
    }


    /**
     * Calculates the number of neighbors having a given CellState of a cell on
     * position (row, col) on the board
     *
     * Note that a cell has 8 neighbors in total, of which any number between 0 and
     * 8 can be the given CellState. The exception are cells along the boarders of
     * the board: these cells have anywhere between 3 neighbors (in the case of a
     * corner-cell) and 5 neighbors in total.
     *
     * @param x     the x-position of the cell
     * @param y     the y-position of the cell
     * @param state the Cellstate we want to count occurences of.
     * @return the number of neighbors with given state
     */
    private int countNeighbors(int row, int col, CellState state) {
        // TODO
        int count=0;
        for(int feltR = row-1; feltR <= row + 1; feltR++){
            for(int feltC = col-1; feltC<=col+1; feltC++ ){
                CellState mainState = getCellState(feltR, feltC);
                try{
                    if(mainState.equals(state)){
                        if (feltR!=row || feltC!=col){
                            count++;
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


        }

        return count;
    }

    @Override
    public IGrid getGrid() {
        return currentGeneration;
    }
}
