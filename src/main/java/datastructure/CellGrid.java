package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    private int rows;
    private int cols;
    private CellState[][]cellState;


    public CellGrid(int rows, int columns, CellState initialState) {
		// TODO Auto-generated constructor stub
        this.rows = rows;
        this.cols= columns;
        cellState = new CellState[this.rows][cols];
        for(int row=0;row<this.rows;row++){
            for(int col=0;col<cols;col++){
                cellState[row][col]=initialState;
            }
        }
	}

    @Override
    public int numRows() {
        // TODO Auto-generated method stub

        return rows;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        // TODO Auto-generated method stub

        oob_exeption(row,column);

        cellState[row][column]  = element;
    }

    @Override
    public CellState get(int row, int column) {
        // TODO Auto-generated method stub
        oob_exeption(row,column);

        return cellState[row][column];
    }

    @Override
    public IGrid copy() {
        // TODO Auto-generated method stub
        CellGrid copyCell = new CellGrid(rows, cols, null);
        for (int row = 0;row<rows; row++)  {
            for (int col = 0;col<cols; col++)  {
                copyCell.set(row, col,this.get(row,col));
            }
        }
            
        return copyCell;
    }
    private void oob_exeption(int row, int column){

         if(row>=rows || row<0){
            throw new IndexOutOfBoundsException();
         }
        if(column>=cols || column<0) {
             throw new IndexOutOfBoundsException();
        }
    }
}